const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = "debug"; //muestra de debug para arriba

logger.debug("Iniciando aplicación en modo de pruebas");
logger.info("La app ha iniciado correctamente :)");
logger.warn("Falta el archivo config de la app");
logger.error("No se pudo acceder al sistema de archivos del equipo");
logger.fatal("Aplicación no se pudo ejecutar");

//refresco en frío es yo tengo que parar la apliación para hacer cambios y luego la vuelvo a levantar
//refresco en caliente es cuando no necesito parar la aplicación para ver los cambios.


function sumar(x,y){
    return x + y;
}

module.exports = sumar;  //si son varios se meten dentro de un objeto tipo {asda,dsa,sf,sa,sda} pero lo mejor es una clase
//y exportar la clase