# PACKAGE MANAGER

Little project to learn about npm, test libraries and logs.

## Getting Started

In order to execute you will need to use npm start or directly execute de index.js file using node.

### Prerequisites

* Node
* NPM


### Installing

Simply clone the repositorie to a decire directory by using the gitlab link: https://gitlab.com/a348503/package-manager.git

## Running the tests

Run the command ---> npm run test    on the directory.


## Built With

* [NodeJS](https://nodejs.org/) - The technology used 
* [Eslint](https://eslint.org/) - Used for code analysis
* [Mocha](https://mochajs.org/) - Used for testing
* [log4js](https://www.npmjs.com/package/log4js) - Use by the devs for the logs. 

## Contributing

This project doesn´t allow any kind of contribbutions since it's just for practice.

## Versioning

1.0.0

## Authors

* **Gilberto Contreras Conn** - *348503* 


## License

ISC

## Acknowledgments

* **Teacher: S. E. Luis Antonio Ramírez Martínez**
