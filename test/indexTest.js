const sumar = require('../index'); //index.js con el require recibe la función sumar por el export.modules
const assert = require('assert'); //buscar q es assert

//Se van a hacer por inferencia, se dice que va a hacer y si pasa o no sabemos si funciona.


describe("Probar la suma de dos números", () => {
    //Afirmar que cinco más cinco son 10
    //it es el marco para la prueba, es el caso de prueba
    it("Cinco más cinco es 10",()=>{
        //en el equal el primer parámetro es lo debe dar y el segundo el resultado de la función
        assert.equal(10,sumar(5,5));
    });

    //Afirmamos que 5 más 5 no son 55
    it("Cinco más cinco no es 55",()=>{
        assert.notEqual(55,sumar(5,5));
    });

}); //agrupador, el primer parámetro indica el grupo de pruebas que es lo que va a hacer, el segundo es una función flecha con las pruebas
//